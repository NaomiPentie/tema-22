<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DateController extends AbstractController
{
    // /**
    //  * @Route("/date", name="date")
    //  */
    // public function index()
    // {
    //     return $this->render('date/index.html.twig', [
    //         'controller_name' => 'DateController',
    //     ]);
    // }

        /**
     * @Route("/datetime", name="datetime")
     */
    public function displayDataSiOra(){
        date_default_timezone_set('Europe/Bucharest');
        $date = date('Y-m-d');
        $ora = date('h:i:sa');
        return $this->render('date/datetime.html.twig', ['date'=>$date, 'ora'=>$ora]);
    }
}
